from typing import List, Tuple, Dict
from agent.ddqn_agent import DDQNAgent
from agent.dqn_agent import DQNAgent
import numpy as np
from tqdm import tqdm
from game_environment.commons_env import HarvestCommonsEnv, DEFAULT_COLORMAP, MAP
from time import time
import os


def run_episode(nobs: dict,
                env: HarvestCommonsEnv,
                agents: dict,
                learningFromIteration: int,
                ddqn: bool,
                updateEvery: int,
                stepsPerEpisode: int = 1000,
                training: bool = True,
                render: bool = False):
    """
    This function is used to run an entire episode in the env passed in parameters with the agents also given
    in parameters.
    :param nobs: initial observation obtained from the reset of the environment
    :param env: environment
    :param agents: agents used in this game
    :param learningFromIteration: step from which the learning occurs (should be at least of size batch size)
    :param ddqn: bool, if True then update of the target model will be applied
    :param updateEvery: if ddqn set to True then the target is updated every updateFrequency iter
    :param stepsPerEpisode: how many steps are included in one episode
    :param training: bool, if set to True then the agent should learn
    :param render: if the env should render the screen
    :return: the agents used and trained and the social metrics of the episode
    """
    # -- Episode LOOP -- #
    # Normalisation of the observations :
    for agent_name in agents:
        nobs[agent_name] = (nobs[agent_name] / 255.).astype(np.float32)
    previous_nobs = nobs

    for it in tqdm(range(stepsPerEpisode), position=0, leave=True):
        if render:
            env.render()

        if training:
            actions = {agent_name: agents[agent_name].act(nobs[agent_name][None]) for agent_name in agents}
        else:
            actions = {agent_name: agents[agent_name].greedyAction(nobs[agent_name][None]) for agent_name in agents}
        # Performs the action
        nobs, nrew, ndone, _ = env.step(actions)
        env.update_social_metrics(nrew)

        for agent_name in agents:
            nobs[agent_name] = (nobs[agent_name] / 255.).astype(np.float32)
            agents[agent_name].addExperiences(obs=previous_nobs[agent_name], reward=nrew[agent_name],
                                              done=ndone[agent_name], next_obs=nobs[agent_name],
                                              action=actions[agent_name])

        if training and it > learningFromIteration:
            for agent_name in agents:
                agents[agent_name].learn()

        if training and it > learningFromIteration and ddqn and (it + 1) % updateEvery == 0:
            for agent_name in agents:
                agents[agent_name].updateTargetModel()

        previous_nobs = nobs
        social_metrics = env.get_social_metrics(episode_steps=stepsPerEpisode)

    return agents, social_metrics


def training(nbrAgents: int,
             agentSensor: List,
             epsilonDecay: float,
             epsilonMin: float,
             epsilonMax: float,
             learningRate: float,
             discountFactor: float,
             nbrTrainingEpisodes: int,
             memorySize: int,
             stepsPerEpisode: int,
             batchSize: int,
             saveAgent: bool,
             mapSize: str,
             updateFrequency: int,
             learningFrom: int,
             saveEvery: int,
             stopExplorationAt : int,
             typeAgent: str = "dqn",
             savePathWeights: str = "agent_weights",
             decayMethod: str = "exp",
             render=False):
    """
    Training loop of the agent.
    :param nbrAgents: nbr of agents present in a simulation.
    :param agentSensor: radius of the agent sensor (centered around himself)
    :param epsilonDecay: epsilon decay
    :param epsilonMin: epsilon minimum value
    :param epsilonMax: epsilon initial value
    :param learningRate: learning rate of the nn
    :param discountFactor: discount factor
    :param nbrTrainingEpisodes: Nbr of episode that was used to train the agent
    :param memorySize: size of the memory
    :param stepsPerEpisode: number of steps of one episode
    :param batchSize: number of replays fed to the nn taken from the memory
    :param saveAgent: bool, if True, agent weights are saved frequently
    :param mapSize: size of the map
    :param updateFrequency: if ddqn model then the target network is
    :param learningFrom: iteration in an episode from wich the agent start to learn
    :param saveEvery: frequency for saving the agent
    :param stopExplorationAt: For linear epsilon decay it is the number of episodes necessary to reach epsilon min
    :param typeAgent: model of the agent
    :param savePathWeights: Path where the weights should be save
    :param decayMethod: linear decay or exp decay
    :param render: env rendering
    :return: social_metrics of the game
    """
    os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

    # INIT ENV
    env = HarvestCommonsEnv(ascii_map=MAP[mapSize], num_agents=nbrAgents, render=render,
                            agent_view_range=agentSensor[0])  # TODO CHANGER LE VISUAL RADIUS DANS L'ENV
    # INIT AGENTS
    if typeAgent == "dqn":
        agents = {agent_name:
                      DQNAgent(nactions=env.action_space.n,
                               obs_shape=env.observation_space.shape,
                               epsilonDecay=epsilonDecay,
                               epsilonMin=epsilonMin,
                               epsilonMax=epsilonMax,
                               alpha=learningRate,
                               gamma=discountFactor,
                               batch_size=batchSize,
                               stopExploration=stopExplorationAt,
                               memorySize=memorySize) for agent_name in env.agents}
    elif typeAgent == "ddqn":
        agents = {agent_name:
                      DDQNAgent(nactions=env.action_space.n,
                                obs_shape=env.observation_space.shape,
                                epsilonDecay=epsilonDecay,
                                epsilonMin=epsilonMin,
                                epsilonMax=epsilonMax,
                                alpha=learningRate,
                                gamma=discountFactor,
                                batch_size=batchSize,
                                stopExploration=stopExplorationAt,
                                memorySize=memorySize) for agent_name in env.agents}
    else:
        raise NotImplementedError("For now only two types of agents are implemented 'dqn' and 'ddqn' other inputs are "
                                  "not allowed for the time being.")
    # CREATE IF NECESSARY CERTAIN FILES
    if saveAgent:
        for agent_name in agents:
            if not os.path.exists(f"{savePathWeights}/{agent_name}") or not os.path.isdir(
                    f"{savePathWeights}/{agent_name}"):
                os.makedirs(f"{savePathWeights}/{agent_name}")

    metrics_training = np.empty((4, nbrTrainingEpisodes))
    # TRAINING LOOP
    for episode in range(nbrTrainingEpisodes):
        # - Start of a new episode - Game and Board reset - #
        print(f"\n--- Starting Episode {episode + 1}/{nbrTrainingEpisodes} ---")
        start_time = time()

        nobs = env.reset()
        for agent_name in agents:
            agents[agent_name].resetMemory()
            if decayMethod == "exp":
                agents[agent_name].decayEpsilon()
            elif decayMethod == "linear":
                agents[agent_name].decayEpsilonLinearly()
            else:
                raise NotImplementedError(f"This epsilon decay method = {decayMethod} was not implemented yet."
                                          f"\nPlease for now use either 'exp' or 'linear'")

        # RUN AN EPISODE AND RETURN THE SOCIAL METRICS FOR THIS EPISODE
        agents, social_metrics = run_episode(nobs=nobs,
                                             env=env,
                                             agents=agents,
                                             stepsPerEpisode=stepsPerEpisode,
                                             training=True,
                                             render=render,
                                             updateEvery=updateFrequency,
                                             learningFromIteration=learningFrom,
                                             ddqn=(typeAgent == "ddqn")
                                             )

        agent_name = "agent-0"
        metrics_training[:, episode] = social_metrics

        print(f"\n--- End of Episode {episode + 1}/{nbrTrainingEpisodes} ---"
              f"\n\t --> Summary of the episode :"
              f"\n\t\t time of the episode : {(time() - start_time) / 60} minutes"
              f"\t -Efficiency = {social_metrics[0]}"
              f"\t -Equitability = {social_metrics[1]}"
              f"\t -Sustainability = {social_metrics[2]}"
              f"\t -Peacefulness = {social_metrics[3]}"
              f"\t - EpsilonValue = {agents[agent_name].epsilon}")

        if saveAgent and (episode + 1) % saveEvery == 0:
            for a in agents:
                agents[a].save(f"{savePathWeights}/{a}/{episode}.h5")

    return agents, metrics_training
