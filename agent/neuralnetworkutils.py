import gym
from tensorflow.keras import Sequential
from tensorflow.keras.layers import Dense, Conv2D, MaxPool2D, Flatten, InputLayer
from typing import Tuple


def returnDenseModel(input_shape: Tuple[int, int, int], output_shape: int) -> Sequential:
    """
    Returns the neural network for the learning agents. This version is a dense model such as described in the original paper.
    :param input_shape: input shape for the neural network
    :param output_shape: output shape for the neural network
    :return: the sequential model
    """
    model = Sequential()
    model.add(InputLayer(input_shape=(input_shape[0], input_shape[1], 3)))
    model.add(Flatten())
    model.add(Dense(32, activation="relu"))
    model.add(Dense(32, activation="relu"))
    model.add(Dense(output_shape))
    return model


def returnConvModel(input_shape: Tuple[int, int, int], output_shape: int) -> Sequential:
    """
    Returns the neural network for the learning agents. This version is a convolutional model more adapted to the input of the CPR game.
    :param input_shape: input shape for the neural network
    :param output_shape: output shape for the neural network
    :return: the sequential model
    """
    model = Sequential()
    model.add(InputLayer((input_shape[0], input_shape[1], 3)))
    model.add(Conv2D(32, kernel_size=3, activation="relu", input_dim=input_shape))
    model.add(MaxPool2D())
    model.add(Conv2D(16, kernel_size=3, activation="relu"))
    model.add(Flatten())
    model.add(Dense(output_shape))
    return model


def returnLightConvModel(input_shape: Tuple[int, int, int], output_shape: int) -> Sequential:
    """
    Returns the neural network for the learning agents. This version was inspired from the following :
    Apraez, D. (2020).   The commons game.https://github.com/Danfoa/commons_game.
    :param input_shape: input shape for the neural network
    :param output_shape: output shape for the neural network
    :return: the sequential model
    """
    model = Sequential()
    model.add(InputLayer((input_shape[0], input_shape[1], 3)))
    model.add(Conv2D(6, kernel_size=3, strides=(1, 1), padding="same", activation="relu", input_dim=input_shape))
    model.add(Conv2D(6, kernel_size=3, strides=(1, 1), padding="same", activation="relu"))
    model.add(Flatten())
    model.add(Dense(32, activation="relu"))
    model.add(Dense(output_shape))
    return model



