from game_environment.commons_env import HarvestCommonsEnv, MAP
from game_environment.utils import utility_funcs
from agent.dqn_agent import DQNAgent
from agent.ddqn_agent import DDQNAgent
from typing import List
import os
from tqdm import tqdm
import shutil


def render_episode(nbrAgents: int,
                   agentSensor: List,
                   stepsPerEpisode: int,
                   mapSize: str,
                   saveNumber: int,
                   savePathVideo: str,
                   typeAgent: str = "dqn",
                   weightsPath: str = "agent_weights"):
    os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
    env = HarvestCommonsEnv(ascii_map=MAP[mapSize], num_agents=nbrAgents, render=True,
                            agent_view_range=agentSensor[0])  # TODO CHANGER LE VISUAL RADIUS DANS L'ENV
    if not os.path.exists(savePathVideo) and not os.path.isdir(savePathVideo):
        os.makedirs(savePathVideo)

    temp_image_folder = f"{savePathVideo}/episode_render"
    try :
        os.mkdir(temp_image_folder)
    except FileExistsError:
        print(f"directory {temp_image_folder} already exist")
    except OSError as err:
        print(err)

    if typeAgent == "dqn":
        agents = {agent_name: DQNAgent(nactions=env.action_space.n,
                                       obs_shape=env.observation_space.shape,
                                       epsilonDecay=0,
                                       epsilonMin=0,
                                       epsilonMax=0,
                                       alpha=0.01,
                                       gamma=0,
                                       batch_size=0,
                                       stopExploration=1,
                                       memorySize=0) for agent_name in env.agents}
    elif typeAgent == "ddqn":
        agents = {agent_name: DDQNAgent(nactions=env.action_space.n,
                                        obs_shape=env.observation_space.shape,
                                        epsilonDecay=0,
                                        epsilonMin=0,
                                        epsilonMax=0,
                                        alpha=0.01,
                                        gamma=0,
                                        batch_size=0,
                                        stopExploration=1,
                                        memorySize=0) for agent_name in env.agents}

    for agent_name in agents:
        agents[agent_name].load(f"{weightsPath}/{agent_name}/{saveNumber}.h5")

    obs = env.reset()

    for t in tqdm(range(stepsPerEpisode)):
        actions = {agent_name: agents[agent_name].act(obs[agent_name][None]) for agent_name in agents}
        obs, rewards, done, infos = env.step(actions)
        env.render(f"{temp_image_folder}/t={t : 05d}.png", title="t=%05d" % t)

    utility_funcs.make_video_from_image_dir(vid_path=savePathVideo, img_folder=temp_image_folder,
                                            video_name="episode=%04d" % saveNumber, fps=10)
    shutil.rmtree(temp_image_folder, ignore_errors=True)


if __name__ == "__main__":
    # PARAMETERS TO SET FOR THE RENDERING OF THE VIDEO
    NBRAGENTS = 1  # THIS PARAMETER IS THE ONE TO PRECISE WHAT WAS THE NUMBER OF AGENT
    AGENTSENSOR = (5, 5)  # THIS IS THE AGENT SENSOR USED IN THE EXPERIMENT
    STEPPEREPISODE = 1000  # THIS IS THE NUMBER OF STEP TO SIMULATE FOR THE EPISODE
    MAPSIZE = "small"  # THIS IS THE SIZE OF THE MAP USE FOR THE RENDERING
    NAMEOFEPISODEWHEREWEIGHTSSAVED = 19  # THIS CORRESPOND TO THE NAME OF THE WEIGHTS SAVED FOR EACH AGENTS
    SAVEVIDEOFOLDERPATH = "policy_single_agent"  # THIS IS THE PATH WHERE THE VIDEO WILL BE RENDERED
    TYPEOFAGENTSUSED = "ddqn"  # THIS IS THE TYPE OF AGENT THAT WAS USED DURING THE EXPERIMENT
    SAVEDWEIGHTSFOLDER = "single_agent_run"  # THIS IS THE MAIN FOLDER WHERE THE WEIGHTS WERE SAVED
    render_episode(nbrAgents=NBRAGENTS,
                   agentSensor=AGENTSENSOR,
                   stepsPerEpisode=STEPPEREPISODE,
                   mapSize=MAPSIZE,
                   saveNumber=NAMEOFEPISODEWHEREWEIGHTSSAVED,
                   savePathVideo=SAVEVIDEOFOLDERPATH,
                   typeAgent=TYPEOFAGENTSUSED,
                   weightsPath=SAVEDWEIGHTSFOLDER)
