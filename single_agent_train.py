from training_runner import training
from matplotlib import pyplot as plt
from datetime import datetime

if __name__ == "__main__":
    nbrAgent = 1
    agentSensor = (5, 5)
    epsilonDecay = .999
    epsilonMin = 0.05
    epsilonMax = 0.25
    learningRate = 0.001
    discountFactor = 0.99
    nbrTrainingEpisodes = 300
    typeAgent = "ddqn"
    stepsPerEpisode = 1000
    batchSize = 16
    updateFrequency = 20
    saveFrequency = 20
    render = False
    saveAgentsWeights = True
    mapSize = "small"
    memorySize = 200
    learningFrom = 100
    savePathWeights = "single_agent_run"
    stopExploration = 140
    decayMethod = "exp"

    agents, social_metrics = training(nbrAgents=nbrAgent,
                                      agentSensor=agentSensor,
                                      epsilonDecay=epsilonDecay,
                                      epsilonMin=epsilonMin,
                                      epsilonMax=epsilonMax,
                                      learningRate=learningRate,
                                      discountFactor=discountFactor,
                                      nbrTrainingEpisodes=nbrTrainingEpisodes,
                                      stepsPerEpisode=stepsPerEpisode,
                                      batchSize=batchSize,
                                      saveAgent=saveAgentsWeights,
                                      mapSize=mapSize,
                                      memorySize=memorySize,
                                      learningFrom=learningFrom,
                                      render=render,
                                      updateFrequency=updateFrequency,
                                      saveEvery=saveFrequency,
                                      typeAgent=typeAgent,
                                      decayMethod=decayMethod,
                                      savePathWeights=savePathWeights,
                                      stopExplorationAt=stopExploration
                                      )

    efficiency_training = social_metrics[0, :]
    equality_training = social_metrics[1, :]
    sustainability_training = social_metrics[2, :]
    peacefulness_training = social_metrics[3, :]

    timestamp = datetime.now().strftime("%d-%b-%Y_(%H-%M-%S-%f)")

    fig = plt.figure(figsize=(8, 6))
    plt.title("Single Agent Training")
    plt.xlabel("Episode")
    plt.ylabel("Efficiency (U)")
    plt.plot(range(nbrTrainingEpisodes), efficiency_training)
    plt.savefig(f"single_agent_training_efficiency_{timestamp}.png")

    fig = plt.figure(figsize=(8, 6))
    plt.title("Single Agent Training")
    plt.xlabel("Episode")
    plt.ylabel("Equality")
    plt.plot(range(nbrTrainingEpisodes), equality_training)
    plt.savefig(f"single_agent_training_equality_{timestamp}.png")

    fig = plt.figure(figsize=(8, 6))
    plt.title("Single Agent Training")
    plt.xlabel("Episode")
    plt.ylabel("Sustainability")
    plt.plot(range(nbrTrainingEpisodes), sustainability_training)
    plt.savefig(f"single_agent_training_sustainability_{timestamp}.png")

    fig = plt.figure(figsize=(8, 6))
    plt.title("Single Agent Training")
    plt.xlabel("Episode")
    plt.ylabel("Peacefulness")
    plt.plot(range(nbrTrainingEpisodes), peacefulness_training)
    plt.savefig(f"single_agent_training_peacefulness_{timestamp}.png")